# Helpful commands:

# read stream
kafka-console-consumer --zookeeper 172.18.18.4:2181 --topic test-topic

# write to kafka
kafka-console-producer --broker-list 172.18.18.4:9092 --topic fluent.info < /var/log/boot.log


# send http to fluentd:
curl -X POST -d 'json={"json":"It is the test massege "}' http://localhost:8888/debug.test

# ___________________________________________________________________________