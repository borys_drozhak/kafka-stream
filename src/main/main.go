package main

import (
	"github.com/Shopify/sarama"
	"fmt"
	"log"
	"os"
	"sync"
	"flag"
	"strings"
	"os/signal"
)

var partitionConsumers = make([] sarama.PartitionConsumer, 1)
var partitionConsumerMtx sync.Mutex

var messageAggregator = make(chan *sarama.ConsumerMessage, 1024)

var systemSignal = make(chan os.Signal)

var consumer sarama.Consumer

var brokerListFlagName = "broker-list"
var defaultBroker = "localhost:9092"
var brokerListVal = flag.String(brokerListFlagName, defaultBroker, "Provide list of brokers(of the same Kafka cluser) to be listening by consumer divided by space")
var brokerList []string

var topicListFlagName = "topic-list"
var topicListVal = flag.String(topicListFlagName, "", "Provide list of topics to be listening by consumer divided by space")
var topicList []string

func main() {
	log.SetOutput(os.Stderr)
	closed := make(chan interface{})
	go listenSystemToExit(closed)
	kafkaConsumer(closed)
}

func listenSystemToExit(closed <-chan interface{}) {
	signal.Notify(systemSignal, os.Interrupt, os.Kill)
	<-systemSignal
	log.Println("Stopping to consume...")

	partitionConsumerMtx.Lock()
	for _, partitionConsumer := range partitionConsumers {
		if partitionConsumer != nil {
			partitionConsumer.Close()
		}
		log.Println("Partition consumer stopped")
	}
	partitionConsumerMtx.Unlock()

	consumer.Close()
	log.Println("Consumer stopped")
	close(messageAggregator)
	<-closed
	os.Exit(0)
}

func kafkaConsumer(closed chan<- interface{}) {
	processInputParameters()

	config := sarama.NewConfig()
	config.Version = sarama.V0_10_0_1
	config.Consumer.Return.Errors = true
	var err error
	consumer, err = sarama.NewConsumer(brokerList, config)
	if err != nil {
		log.Println("Can't create consumer because of: " + err.Error())
		os.Exit(1)
	}

	outDone := make(chan interface{})
	go outputMessages(outDone)

	log.Println("Topics to listen: ", topicList)
	for _, topic := range topicList {
		go processTopic(consumer, topic)
	}

	closed <- <-outDone
}

func outputMessages(outDone chan<- interface{}) {
	for message := range messageAggregator {
		fmt.Println(string(message.Value[:]))
	}
	log.Println("All output done")
	outDone <- nil
}

func splitParamList(arg string, sep string) [] string {
	res := make([]string, 0)
	for _, str := range strings.Split(arg, sep) {
		res = append(res, strings.TrimSpace(str))
	}
	fmt.Println(res)
	return res
}

func processInputParameters() {
	flag.Parse()

	brokerList = splitParamList(*brokerListVal, ",")
	if brokerList[0] == "" {
		log.Printf("The default broker will be used: %s\n", defaultBroker)
	}

	topicList = splitParamList(*topicListVal, ",")
	if topicList[0] == "" {
		topicListFlag := flag.Lookup(topicListFlagName)
		log.Printf("%s\n", topicListFlag.Usage)
		os.Exit(1)
	}
}

func processTopic(consumer sarama.Consumer, topic string) {
	log.Println("Consume messages from topic: ", topic)
	partitionsIds, err := consumer.Partitions(topic)
	if err != nil {
		log.Printf("Can't get partitions for topic: %s because of %s", topic, err.Error())
		return
	}

	var wg = new(sync.WaitGroup)
	for _, partitionId := range partitionsIds {
		wg.Add(1)
		go processPartition(consumer, topic, partitionId, messageAggregator, wg)
	}
	wg.Wait()
	log.Println("Done with topic: ", topic)
}

func processPartition(consumer sarama.Consumer,
					topic string,
					partitionId int32,
					messageAggregator chan<- *sarama.ConsumerMessage,
					wg *sync.WaitGroup) {
	log.Printf("Consume messages from topic: %s partition: %d", topic, partitionId)
	partitionConsumer, err := consumer.ConsumePartition(topic, partitionId, sarama.OffsetNewest)
	if err != nil {
		log.Printf("Can't consume messages for topic: %s with partition: %d, because of %s\n", topic, partitionId, err.Error())
		wg.Done()
		return
	}

	partitionConsumerMtx.Lock()
	partitionConsumers = append(partitionConsumers, partitionConsumer)
	partitionConsumerMtx.Unlock()

	messageReader := partitionConsumer.Messages()
	errorsReader := partitionConsumer.Errors()
	loop: for {
		select {
		case message := <-messageReader:
			messageAggregator <- message
		case err := <-errorsReader:
			log.Printf("Received an error: %s for topic: %s and partition: %d", err.Error(), err.Topic, err.Partition)
			break loop
		}
	}

	partitionConsumerMtx.Lock()
	for indx, pc := range partitionConsumers {
		if pc == partitionConsumer {
			partitionConsumers = append(partitionConsumers[0:indx], partitionConsumers[indx+1:]...)
		}
	}
	partitionConsumerMtx.Unlock()

	wg.Done()
	log.Printf("Done with partition: %d of topic: %s\n", partitionId, topic)
}