**Usage**
To build executable binary for Linux system run build.bat with parameter _linux_
To build executable binary for Windows system run build.bat without parameters.

To read data from two topics: fluent.info and error.info. run command:
_kafka-stream_ _-broker-list_ _172.18.18.4:9092,172.18.18.4:9093_ _-topic-list_ _fluent.info,error.info_
Where broker-list is a list of brokers of one cluster(can be only one)

**Produced Output**
The output of errors is STDERR stream
The output of data is STDOUT stream