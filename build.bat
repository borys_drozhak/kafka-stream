set name="kafka-stream"
if "%1"=="" (
    set GOOS=windows
    set name="%name%.exe"
)
if "%1"=="windows" (
    set GOOS=windows
    set name=%name%.exe"
)
if "%1"=="linux" (
    set GOOS=linux
)

set GOPATH=%cd%
go get -t github.com/Shopify/sarama
go build -o %name% src/main/main.go